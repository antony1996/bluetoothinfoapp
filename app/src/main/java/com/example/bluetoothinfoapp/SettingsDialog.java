package com.example.bluetoothinfoapp;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import static com.example.bluetoothinfoapp.MainActivity.timeOfDeviceScann;

/**
 * Created by Антон on 21.02.2017.
 */

public class SettingsDialog extends DialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.settings_dialog, null);

        final EditText timeOfDeviceScan = (EditText) view.findViewById(R.id.editText_scann_sec);

        timeOfDeviceScan.setText(String.valueOf(timeOfDeviceScann));

        timeOfDeviceScan.setSelection(timeOfDeviceScan.getText().length());

        builder.setTitle("Настройки");
        Button bSave = (Button) view.findViewById(R.id.save);
        bSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                timeOfDeviceScann = Integer.parseInt(timeOfDeviceScan.getText().toString());

                if(((MainActivity)getActivity()).timerForBluetoothDeviceSearch != null) {

                    ((MainActivity) getActivity()).timerForBluetoothDeviceSearch.cancel();
                    ((MainActivity) getActivity()).startTimerForBluetoothDeviceSearch(timeOfDeviceScann);
                }

                dismiss();
            }
        });

        builder.setView(view);

        final AlertDialog alert = builder.create();
        alert.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        return alert;

    }
}
