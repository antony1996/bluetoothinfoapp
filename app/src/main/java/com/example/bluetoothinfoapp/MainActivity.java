package com.example.bluetoothinfoapp;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.karumi.dexter.Dexter;

import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import static android.bluetooth.BluetoothAdapter.getDefaultAdapter;
import static android.os.Build.VERSION_CODES.M;
import static com.example.bluetoothinfoapp.DataBase.NULL;
import static com.example.bluetoothinfoapp.DataBase.TRUE;

public class MainActivity extends AppCompatActivity {

    private BluetoothAdapter mBluetoothAdapter;
    private WifiManager wifiManager;
    private TextView bluetoothName, bluetoothMac, wifiName, wifiMac, wifiIP;
    private ListView listView;
    private ArrayAdapter arrayAdapter;
    public RelativeLayout relativeLayout;
    private boolean startFromButton = false;
    DataBase dataBase;
    ProgressBar progressBar;

    CountDownTimer timerForBluetoothDeviceSearch;

    //    Задержка на поиск устройст блютуз
    public static int timeOfDeviceScann = 10;

    private ArrayList<BluetoothModel> list = new ArrayList<>();

    private HashMap <String, String> listDevisesON = new HashMap<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        dataBase =  new DataBase(getApplicationContext());
        dataBase.openOfCreateDatabase();

        list = dataBase.getAllBluetooth();

//        Инициализация библиотеки для проверки разрешения на андроид версии 6 и выше
        Dexter.initialize(getApplicationContext());
//          Проверка разрешений
        if (M <= Build.VERSION.SDK_INT)
            new Permissions(getApplicationContext()).checkForPermision();

        mBluetoothAdapter = getDefaultAdapter();
        wifiManager = (WifiManager) getBaseContext().getSystemService(Context.WIFI_SERVICE);

        initializationView();

        arrayAdapter = new AdapterForBluetooth(this, R.layout.listview_item, list);
        listView.setAdapter(arrayAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();

        progressBar.setVisibility(View.GONE);

        getWifiInfo();

        getBluetoothInfo();

        IntentFilter intentFilter = new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
        intentFilter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        registerReceiver(discoveryMonitor, intentFilter);
        registerReceiver(discoveryResult, new IntentFilter(BluetoothDevice.ACTION_FOUND));

        startSearchDevises();
    }

     void startTimerForBluetoothDeviceSearch(int time){
         timerForBluetoothDeviceSearch = new CountDownTimer(time * 1000, 1000) {

            @Override
            public void onTick(long millisUntilFinished) {}

            @Override
            public void onFinish() {
                if(!mBluetoothAdapter.isDiscovering()) {
                    startFromButton = false;
                    startSearchDevises();
                }
            }

        }.start();
    }


    //    Отслеживаем состояние загрузки началась/закончилась (блютуз)
    BroadcastReceiver discoveryMonitor = new BroadcastReceiver() {
        String dStarted = BluetoothAdapter.ACTION_DISCOVERY_STARTED;
        String dFinished = BluetoothAdapter.ACTION_DISCOVERY_FINISHED;

        @Override
        public void onReceive(Context context, Intent intent) {
            if (dStarted.equals(intent.getAction())) {

                listDevisesON.clear();

                if(startFromButton)
                    progressBar.setVisibility(View.VISIBLE);

            } else if (dFinished.equals(intent.getAction())) {
                progressBar.setVisibility(View.GONE);

                startTimerForBluetoothDeviceSearch(timeOfDeviceScann);

                for(int i = 0; i < list.size(); i ++){
                    if(listDevisesON.containsValue(list.get(i).getMacAdress())){
                        list.get(i).setOnline(true);
                    } else {
                        list.get(i).setOnline(false);
                    }
                }

                Collections.sort(list, new ScoreComparator());
                arrayAdapter.notifyDataSetChanged();

            }
        }
    };


//    Получаем данные о устройствах рядом при включении блютуза
    private BroadcastReceiver bluetoothReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String stateExtra = BluetoothAdapter.EXTRA_STATE;
            int state = intent.getIntExtra(stateExtra, -1);

            if (state == BluetoothAdapter.STATE_ON) {
                startSearchDevises();
                unregisterReceiver(this);
            }
        }
    };


//    Добавляем данные которые пришли в список (блютуз)
            BroadcastReceiver discoveryResult = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String remoteDeviceName = intent.getStringExtra(BluetoothDevice.EXTRA_NAME);
            BluetoothDevice remoteDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
            String remoteDeviceMacAdress = remoteDevice.getAddress();

            if(remoteDeviceName != null && remoteDeviceMacAdress != null) {

                int createOrNot[] = dataBase.getBluetoothApp(remoteDeviceMacAdress);

                if (startFromButton) {

                    if (createOrNot[0] == NULL && !remoteDeviceName.isEmpty() && !remoteDeviceMacAdress.isEmpty()) {
                        dataBase.insertDevise(remoteDeviceName, remoteDeviceMacAdress);
                        list.add(new BluetoothModel(remoteDeviceName, remoteDeviceMacAdress, true));
                        listDevisesON.put(remoteDeviceMacAdress, remoteDeviceMacAdress);
                    } else {
                        list.get(createOrNot[1]).setOnline(true);
                        listDevisesON.put(remoteDeviceMacAdress, remoteDeviceMacAdress);
                    }

                } else {
                    if (createOrNot[0] == TRUE) {
                        listDevisesON.put(remoteDeviceMacAdress, remoteDeviceMacAdress);
                    }
                }
            }

        }
    };

    //    Сортировка списка
    class ScoreComparator implements Comparator<BluetoothModel> {

        @Override
        public int compare(BluetoothModel objA, BluetoothModel objB) {
            boolean b1 = objB.getIsOnline();
            boolean b2 = objA.getIsOnline();
            if( b1 && ! b2 ) {
                return +1;
            }
            if( ! b1 && b2 ) {
                return -1;
            }
            return 0;
        }
    }

    //      Получить данные по блютуз и записать их в textView
    private void getBluetoothInfo() {
        if (getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH)) {
            if (mBluetoothAdapter.getState() == BluetoothAdapter.STATE_ON) {
                // Добавляем данные в textView
                setBluetoothData();
            } else {
                bluetoothName.setText("bluetooth выключен");
                bluetoothMac.setText("bluetooth выключен");

                registerReceiver(new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        String stateExtra = BluetoothAdapter.EXTRA_STATE;
                        int state = intent.getIntExtra(stateExtra, -1);

                        if (state == BluetoothAdapter.STATE_ON) {
                            // Добавляем данные в textView
                            setBluetoothData();
                            unregisterReceiver(this);
                        }
                    }
                }, new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED));
//                 Включаем блютуз
                mBluetoothAdapter.enable();
            }

        } else {
            Toast.makeText(this, "Bluetooth не поддерживается", Toast.LENGTH_SHORT).show();
        }
    }

    private void initializationView() {
        relativeLayout = (RelativeLayout) findViewById(R.id.relativeLayout);

        bluetoothName = (TextView) findViewById(R.id.bluetooth_name);
        bluetoothMac = (TextView) findViewById(R.id.bluetooth_mac);

        wifiName = (TextView) findViewById(R.id.wifi_name);
        wifiMac = (TextView) findViewById(R.id.wifi_mac);
        wifiIP = (TextView) findViewById(R.id.wifi_ip);

        listView = (ListView) findViewById(R.id.listView);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);
    }

    private String getDeviceCurrentIPAddress() {
        int ip = wifiManager.getConnectionInfo().getIpAddress();
        String ipString = String.format("%d.%d.%d.%d", (ip & 0xff), (ip >> 8 & 0xff), (ip >> 16 & 0xff), (ip >> 24 & 0xff));
        return ipString;
    }

    private String getMacAddres() {
        try {
            List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface nif : all) {
                if (!nif.getName().equalsIgnoreCase("wlan0")) continue;

                byte[] macBytes = nif.getHardwareAddress();
                if (macBytes == null) {
                    return "";
                }

                StringBuilder res1 = new StringBuilder();
                for (byte b : macBytes) {
                    res1.append(String.format("%02X:", b));
                }

                if (res1.length() > 0) {
                    res1.deleteCharAt(res1.length() - 1);
                }
                return res1.toString();
            }
        } catch (Exception ex) {
        }
        return "02:00:00:00:00:00";
    }

//    если вайфай меняет сеть, то перезаписывать элементы textView
    private BroadcastReceiver wifiReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (wifiManager.getConnectionInfo() != null) {
                wifiName.setText(wifiManager.getConnectionInfo().getSSID());
                wifiIP.setText(getDeviceCurrentIPAddress());
            }
        }
    };

    private void getWifiInfo() {
        wifiMac.setText(getMacAddres());

        if (wifiManager.isWifiEnabled()) {
            if (wifiManager.getConnectionInfo() != null) {
                wifiName.setText(wifiManager.getConnectionInfo().getSSID());
                wifiIP.setText(getDeviceCurrentIPAddress());
            }
        } else {
            registerReceiver(wifiReceiver, new IntentFilter("android.net.wifi.STATE_CHANGE"));
//                Включение WiFi
            wifiManager.setWifiEnabled(true);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id){
            case R.id.action_search_devises:
                if(mBluetoothAdapter.isDiscovering())
                    mBluetoothAdapter.cancelDiscovery();

                    startFromButton = true;
                    startSearchDevises();
                break;

            case R.id.action_settings:
                SettingsDialog settingsDialog = new SettingsDialog();
                settingsDialog.show(getFragmentManager(), "settingsDialog");

                break;

            case R.id.action_info:
                if(relativeLayout.getVisibility() == View.VISIBLE)
                relativeLayout.setVisibility(View.GONE);
                else
                    relativeLayout.setVisibility(View.VISIBLE);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setBluetoothData (){
        String macAddress = android.provider.Settings.Secure.getString(this.getContentResolver(), "bluetooth_address");

        if(Build.VERSION_CODES.JELLY_BEAN_MR2 >= Build.VERSION.SDK_INT){
            macAddress = BluetoothAdapter.getDefaultAdapter().getAddress();
        }

        bluetoothName.setText(mBluetoothAdapter.getName());
        bluetoothMac.setText(macAddress);
    }

    private void startSearchDevises(){

        if (!mBluetoothAdapter.isEnabled())
            mBluetoothAdapter.enable();

        if(!mBluetoothAdapter.isDiscovering())
            mBluetoothAdapter.startDiscovery();

    }

    @Override
    protected void onPause() {
        super.onPause();

        if(timerForBluetoothDeviceSearch != null)
            timerForBluetoothDeviceSearch.cancel();

        if(mBluetoothAdapter.isDiscovering())
            mBluetoothAdapter.cancelDiscovery();

        try {
            unregisterReceiver(wifiReceiver);
            unregisterReceiver(bluetoothReceiver);
            unregisterReceiver(discoveryResult);
            unregisterReceiver(discoveryMonitor);

        } catch (IllegalArgumentException e) {

        }

    }
}

