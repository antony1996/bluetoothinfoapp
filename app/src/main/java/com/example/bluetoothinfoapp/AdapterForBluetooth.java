package com.example.bluetoothinfoapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Антон on 21.02.2017.
 */

public class AdapterForBluetooth extends ArrayAdapter<BluetoothModel> {

    Context context;
    ArrayList<BluetoothModel> deviseArray;

    public AdapterForBluetooth(Context context, int resource, ArrayList<BluetoothModel> deviseArray) {
        super(context, resource, deviseArray);
        this.context = context;
        this.deviseArray = deviseArray;
    }

    static class ViewHolder {
        TextView name;
        TextView macAdress;
        ImageView imageView;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;

        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.listview_item, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.name = (TextView) convertView.findViewById(R.id.name);
            viewHolder.macAdress = (TextView) convertView.findViewById(R.id.mac_adress);
            viewHolder.imageView = (ImageView) convertView.findViewById(R.id.imageView);
            convertView.setTag(viewHolder);
        }
        else{
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.name.setText(deviseArray.get(position).getName());
        viewHolder.macAdress.setText(deviseArray.get(position).getMacAdress());

        if(deviseArray.get(position).getIsOnline()){
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP)
                viewHolder.imageView.setImageDrawable(context.getDrawable(R.drawable.green));
            else
                viewHolder.imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.green));
        }else {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP)
                viewHolder.imageView.setImageDrawable(context.getDrawable(R.drawable.red));
            else
                viewHolder.imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.red));
        }



        return convertView;
    }

}
