package com.example.bluetoothinfoapp;

/**
 * Created by Антон on 21.02.2017.
 */

public class BluetoothModel {

    private String name;
    private String macAdress;
    private boolean isOnline;

    public BluetoothModel(String name, String macAdress, boolean isOnline) {
        this.name = name;
        this.macAdress = macAdress;
        this.isOnline = isOnline;
    }

    public String getName() {
        return name;
    }

    public String getMacAdress() {
        return macAdress;
    }

    public boolean getIsOnline() {
        return isOnline;
    }

    public void setOnline(boolean online) {
        isOnline = online;
    }
}
