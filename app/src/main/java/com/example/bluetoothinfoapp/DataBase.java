package com.example.bluetoothinfoapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

/**
 * Created by Антон on 27.02.2017.
 */

public class DataBase {
    private final String DATA_BASE_NAME = "RegistredBluetoothDevises.db";
    private final String BLUETOOTH_TABLE = "RegistredBluetooth";
    private final String BLUETOOTH_NAME_COLUMN = "name";
    private final String BLUETOOTH_MAC_ADRESS_COLUMN = "mac_adress";

    public static final int NULL = -1;
    public static final int TRUE = 1;

    private Context context;

    public DataBase(Context context){
        this.context = context;
    }

    public void openOfCreateDatabase() {
        SQLiteDatabase database = context.openOrCreateDatabase(DATA_BASE_NAME, Context.MODE_PRIVATE, null);
        database.execSQL("CREATE TABLE IF NOT EXISTS " + BLUETOOTH_TABLE +
                " (_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                BLUETOOTH_NAME_COLUMN + " STRING," +
                BLUETOOTH_MAC_ADRESS_COLUMN + " STRING" +
                ");");
        database.close();
    }

    public int [] getBluetoothApp(String macAdress) {
        int auth [] = {NULL, NULL};
        SQLiteDatabase database = context.openOrCreateDatabase(DATA_BASE_NAME, Context.MODE_PRIVATE, null);
        Cursor cursor = database.query(BLUETOOTH_TABLE, null, BLUETOOTH_MAC_ADRESS_COLUMN + " = ?", new String[]{macAdress}, null, null, null);
        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            if(cursor.getString(2).isEmpty()) {
                auth[0] = NULL;
                auth[1] = cursor.getPosition();
            }else {
                auth [0] = TRUE;
                auth [1] = cursor.getPosition();
            }
            cursor.moveToNext();
        }
        cursor.close();
        database.close();
        return auth;
    }

    public void insertDevise (String name, String macAdress) {
        SQLiteDatabase database = context.openOrCreateDatabase(DATA_BASE_NAME, Context.MODE_PRIVATE, null);
        ContentValues values = new ContentValues();
        values.put(BLUETOOTH_NAME_COLUMN, name);
        values.put(BLUETOOTH_MAC_ADRESS_COLUMN, macAdress);
        database.insert(BLUETOOTH_TABLE, null, values);
        database.close();
    }

    ArrayList<BluetoothModel> getAllBluetooth() {
        ArrayList<BluetoothModel> list = new ArrayList<>();
        SQLiteDatabase database = context.openOrCreateDatabase(DATA_BASE_NAME, Context.MODE_PRIVATE, null);
        Cursor cursor = database.query(BLUETOOTH_TABLE, null, null, null, null, null, null);
        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            BluetoothModel disk = new BluetoothModel(cursor.getString(1), cursor.getString(2), false);
            list.add(disk);
            cursor.moveToNext();
        }
        cursor.close();
        database.close();
        return list;
    }

}
